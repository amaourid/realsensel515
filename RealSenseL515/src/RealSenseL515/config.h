#pragma once

#include <sofa/config.h>

#ifdef SOFA_BUILD_RealSenseL515
#  define     RealSenseL515_API SOFA_EXPORT_DYNAMIC_LIBRARY
#else
#  define     RealSenseL515_API SOFA_IMPORT_DYNAMIC_LIBRARY
#endif