#ifndef SOFA_COMPONENT_L515_INL
#define SOFA_COMPONENT_L515_INL

#include <RealSenseL515/PointCloud.h>

namespace sofa
{
    PointCloud::PointCloud()
        : Inherited()
        , d_cloudFile(initData(&d_cloudFile,std::string("/home/maourid/sofaL515.pcd"), "PointCloudFile", "path to save point cloud"))
    {
        // Nothing more is done here
    }

    typedef pcl::PointXYZRGB RGB_Cloud;
    typedef pcl::PointCloud<RGB_Cloud> point_cloud;
    typedef point_cloud::Ptr cloud_pointer;

    void PointCloud::configPipe()
    {
        //Configuring the pipeline with a non default profile
        cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
        cfg.enable_stream(RS2_STREAM_COLOR,1280, 720, RS2_FORMAT_RGB8, 30);

        //Start streaming with the requested configuration
        this->profile = this->pipe.start(cfg);

        // LASER POWER - DIGITAL GAIN - RECEIVER GAIN - MIN DISTANCE
        this->dev = profile.get_device();
        this->sensor = dev.query_sensors().front();
        sensor.set_option(RS2_OPTION_VISUAL_PRESET,RS2_L500_VISUAL_PRESET_SHORT_RANGE);

        this->configured = true;
    }

    void PointCloud::stabilizeAutoExp() 
    {
        for (int i = 30 ; i-- ;) pipe.wait_for_frames();
        this->autoExposure = true;
    }

    void PointCloud:: acquireAlignedFrames()
    {
        // Align object
        rs2::align align(RS2_STREAM_COLOR); 
        // Wait for next set of frames from the camera
        this->data = pipe.wait_for_frames();

        // Align all frames to color viewport
        this->data = align.process(data);
        this->depth = data.get_depth_frame();
        this->RGB = data.get_color_frame();
    }

    void PointCloud::processFrames()
    {
        // Custom processing block to its output queue
        rs2::processing_block pb(
        [](rs2::frame f, rs2::frame_source& src)
        {
            // For each input frame f, do:
            // frame --> cv
            cv::Mat image = PointCloud::frame_to_mat(f);
            
            // do some (silly :p) processing
            // LIVER SEGMENTATION
            // cv::Mat mask = cv::imread("/home/maourid/software/question 6/LiverDataML/masks/mask.png",cv::IMREAD_GRAYSCALE); //LOAD MASK
            // cv::bitwise_not(mask,mask);                                                                                     //INVERT COLORS
            // cv::resize(mask,mask,cv::Size(image.size().width,image.size().height));                                         //RESIZE MASK
            // cv::bitwise_and(image,cv::Scalar(1.f,1.f,1.f),image,mask);                                                      //APPLY MASK

            // Allocating new frame and copying all missing data from f.
            auto res = src.allocate_video_frame(f.get_profile(), f);

            // copy from cv --> frame
            memcpy((void*)res.get_data(), image.data, image.size().width * image.size().height * 2);

            // Send the resulting frame to the output queue
            src.frame_ready(res);
        });
        pb.start(this->q); // Bind output of the processing block to be enqueued into the queue

        // Send depth frame for processing
        pb.invoke(this->depth);
        // Wait for results
        this->depth = this->q.wait_for_frame();

        // Apply filters
        this->depth = dec_filter.process(depth);
        this->depth = spat_filter.process(depth);
        this->depth = temp_filter.process(depth);
    }

    void PointCloud::getPointCloud () 
    {
        // Map Color texture to each point
        pc.map_to(RGB);

        // Generate Point Cloud
        points = pc.calculate(depth);

        // Convert generated Point Cloud to PCL Formatting
        cloud_pointer cloud = PointCloud::PCL_Conversion(points, RGB);

        // Apply PCL filters here if wanted
                
        // Take Cloud Data and write to .PCD File Format
        pcl::io::savePCDFileBinary(d_cloudFile.getValue(), *cloud); // Input cloud to be saved to .pcd
    }

    std::tuple<int, int, int> PointCloud::RGB_Texture(rs2::video_frame texture, rs2::texture_coordinate Texture_XY)
    {
        // Get Width and Height coordinates of texture
        int width  = texture.get_width();  // Frame width in pixels
        int height = texture.get_height(); // Frame height in pixels
        
        // Normals to Texture Coordinates conversion
        int x_value = std::min(std::max(int(Texture_XY.u * width  + .5f), 0), width - 1);
        int y_value = std::min(std::max(int(Texture_XY.v * height + .5f), 0), height - 1);

        int bytes = x_value * texture.get_bytes_per_pixel();   // Get # of bytes per pixel
        int strides = y_value * texture.get_stride_in_bytes(); // Get line width in bytes
        int Text_Index =  (bytes + strides);

        const auto New_Texture = reinterpret_cast<const uint8_t*>(texture.get_data());
        
        // RGB components to save in tuple
        int NT1 = New_Texture[Text_Index];
        int NT2 = New_Texture[Text_Index + 1];
        int NT3 = New_Texture[Text_Index + 2];

        return std::tuple<int, int, int>(NT1, NT2, NT3);
    }

    cloud_pointer PointCloud::PCL_Conversion(const rs2::points& points, const rs2::video_frame& color)
    {
        // Declare Point Cloud
        cloud_pointer cloud(new point_cloud);

        // Declare Tuple for RGB value Storage (<t0>, <t1>, <t2>)
        std::tuple<uint8_t, uint8_t, uint8_t> RGB_Color;

        // Convert data captured from Realsense camera to Point Cloud
        auto sp = points.get_profile().as<rs2::video_stream_profile>();
        
        cloud->width  = static_cast<uint32_t>( sp.width()  );   
        cloud->height = static_cast<uint32_t>( sp.height() );
        cloud->is_dense = false;
        cloud->points.resize( points.size() );

        auto Texture_Coord = points.get_texture_coordinates();
        auto Vertex = points.get_vertices();

        // Iterating through all points and setting XYZ coordinates and RGB values
        for (int i = 0; i < points.size(); i++)
        {   
            // Mapping Depth Coordinates - Depth data stored as XYZ values
            cloud->points[i].x = Vertex[i].x;
            cloud->points[i].y = Vertex[i].y;
            cloud->points[i].z = Vertex[i].z;

            // Obtain color texture for specific point
            RGB_Color = PointCloud::RGB_Texture(color, Texture_Coord[i]);

            // Mapping Color  (BGR due to L515 camera model)
            cloud->points[i].r = std::get<2>(RGB_Color); // Reference tuple<2>
            cloud->points[i].g = std::get<1>(RGB_Color); // Reference tuple<1>
            cloud->points[i].b = std::get<0>(RGB_Color); // Reference tuple<0>
        }
    return cloud; 
    }

    cv::Mat PointCloud::frame_to_mat(const rs2::frame& f)
    {
        using namespace cv;
        using namespace rs2;

        auto vf = f.as<video_frame>();
        const int w = vf.get_width();
        const int h = vf.get_height();

        if (f.get_profile().format() == RS2_FORMAT_BGR8)
        {
            return Mat(cv::Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
        }
        else if (f.get_profile().format() == RS2_FORMAT_RGB8)
        {
            auto r = Mat(cv::Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
            cvtColor(r, r, COLOR_RGB2BGR);
            return r;
        }
        else if (f.get_profile().format() == RS2_FORMAT_Z16)
        {
            return Mat(cv::Size(w, h), CV_16UC1, (void*)f.get_data(), Mat::AUTO_STEP);
        }
        else if (f.get_profile().format() == RS2_FORMAT_Y8)
        {
            return Mat(cv::Size(w, h), CV_8UC1, (void*)f.get_data(), Mat::AUTO_STEP);
        }
        else if (f.get_profile().format() == RS2_FORMAT_DISPARITY32)
        {
            return Mat(cv::Size(w, h), CV_32FC1, (void*)f.get_data(), Mat::AUTO_STEP);
        }
        throw std::runtime_error("Frame format is not supported yet!");
    }

    void PointCloud::init() 
    {
        configPipe();
        stabilizeAutoExp();
        acquireAlignedFrames();
        processFrames();
        getPointCloud();
    }


} // namespace sofa

#endif 