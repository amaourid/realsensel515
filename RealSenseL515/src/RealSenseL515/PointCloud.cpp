#define SOFA_COMPONENT_L515_CPP

#include <RealSenseL515/PointCloud.inl>
#include <sofa/core/ObjectFactory.h>


namespace sofa
{

//// Register in the Factory

int PointCloudClass = core::RegisterObject( "A point cloud streaming component for sofa using RealSense camera LiDAR L515").add<PointCloud>(true);

} // namespace sofa



