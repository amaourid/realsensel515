#ifndef SOFA_COMPONENT_L515_H
#define SOFA_COMPONENT_L515_H

#include<RealSenseL515/config.h>
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/objectmodel/DataCallback.h>
#include <sofa/core/objectmodel/DataFileName.h>
#include <sofa/defaulttype/DataTypeInfo.h>

#include <iostream>
#include <algorithm> 
#include <string>

#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <opencv2/opencv.hpp>   // Include OpenCV API

// PCL Headers
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/common_headers.h>
#include <boost/thread/thread.hpp>
#include <pcl/io/io.h>


using namespace rs2;
using namespace std;

namespace sofa
{
    class PointCloud : public core::objectmodel::BaseObject
    {
    public:
        SOFA_CLASS(PointCloud  , sofa::core::objectmodel::BaseObject);
        typedef core::objectmodel::BaseObject Inherited;

        typedef pcl::PointXYZRGB RGB_Cloud;
        typedef pcl::PointCloud<RGB_Cloud> point_cloud;
        typedef point_cloud::Ptr cloud_pointer;

        //----------------------------------------------------------------------------
        //   Attributes
        //----------------------------------------------------------------------------
        PointCloud();
        // Set path to save the resulted point cloud
        Data<std::string> d_cloudFile ;

        // RealSense pipeline
        rs2::pipeline pipe;

        // Pointcloud objects
        rs2::pointcloud pc;
        rs2::points points;

        // Filters
        rs2::decimation_filter dec_filter;  // Decimation - reduces depth frame density
        rs2::spatial_filter spat_filter;    // Spatial    - edge-preserving spatial smoothing
        rs2::temporal_filter temp_filter;   // Temporal   - reduces temporal noise

        // Pipeline and streaming parameters
        rs2::config cfg;
        rs2::pipeline_profile profile;
        rs2::device dev;
        rs2::sensor sensor;

        // bool to check configuration and auto-exposure
        bool configured = false;
        bool autoExposure = false;

        // Frames objects
        rs2::frameset data;
        rs2::frame depth, RGB;

        // Custom processing block output queue
        rs2::frame_queue q;       

        //----------------------------------------------------------------------------
        //   Main use
        //----------------------------------------------------------------------------
        
        //  Setup realsense data acquisition pipeline
        void configPipe();

        // Drop several frames for auto-exposure
        void stabilizeAutoExp();

        // acquire aligned data
        void acquireAlignedFrames();

        // apply filters 
        void processFrames();

        // map point cloud
        void getPointCloud ();

        /// Initialization function
        void init() override;

    protected:

        //----------------------------------------------------------------------------
        //   Helpers
        //----------------------------------------------------------------------------

        // Extract the RGB data from a single point return R, G, and B values. 
        static std::tuple<int, int, int> RGB_Texture(rs2::video_frame texture, rs2::texture_coordinate Texture_XY);

        // Function is utilized to fill a point cloud object with 
        // depth and RGB data from a single frame captured using the Realsense.
        static cloud_pointer PCL_Conversion(const rs2::points& points, const rs2::video_frame& color);

        // Convert rs2::frame to cv::Mat
        static cv::Mat frame_to_mat(const rs2::frame& f);

    };
}

#endif  // SOFA_COMPONENT_L515_H