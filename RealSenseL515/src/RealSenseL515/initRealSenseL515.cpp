#include <RealSenseL515/config.h>

namespace sofa
{

extern "C" 
{
    void initExternalModule()
    {
        static bool first = true;
        if (first)
        {
            first = false;
        }
    }

    const char* getModuleName()
    {
        return "RealSenseL515";
    }

    const char* getModuleVersion()
    {
        return "0.0";
    }

    const char* getModuleLicense()
    {
        return "LGPL";
    }

    const char* getModuleDescription()
    {
        return "RealSense L515 camera LiDAR plugin in SOFA";
    }
    const char* getModuleComponentList()
    {
        // string containing the names of the classes provided by the plugin
        return "PointCloud";
    }

} // extern "C"

} // namespace sofa
