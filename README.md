# RealSense LiDAR camera L515 plugin in SOFA 

<img src="https://repository-images.githubusercontent.com/60089478/0a5bb280-06cf-11ea-8ae4-864729c400e0  s=800&d=identicon" align="right"
     alt="Size Limit logo by Anton Lovchikov" width="80" height="88">

Sofa component for using realsense LiDAR camera to stream point cloud.

## Dependencies
* Sofa (release v20.12)
* OpenCV
* realsense2
* PCL

## Compile the plugin out of tree 

* mkdir ext_plugin_repo  && cd ext_plugin_repo
* git clone https://gitlab.inria.fr/amaourid/realsensel515
* cd sofa/build 
* cmake-gui ../src/
* Set the value of SOFA_EXTERNAL_DIRECTORIES to the path of ext_plugin_repo folder
* Configure then select PLUGIN_REALSENSEL515, Configure and generate

# Usage and description 

* Connect the realsense camera LiDAR L515 to USB 3 port.
* Here we instantiated a PointCloud component :

```xml
<PointCloud name="pc" PointCloudFile="/path/to/save/pointcloud.pcd" />

```
